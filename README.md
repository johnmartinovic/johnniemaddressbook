# README #

JohnnieMAddressBook is a mini online address book. User can create and manipulate with countries, cities and contacts.

JohnnieMAddressBook can be tested [here](johnniem.com:8080/JohnnieMAddressBook).

# Bugs #
1. When editing some City, error will appear if some existing value is chosen as the new one
2. Validation protocol should include the check of max DB coulumns lengths, as this causes problems when inserting values of bigger length

## Further improvements ##
* 'delete' buttons should send POST requests instead of GET
* introduce some modern JavaScript libraries
* if error occurs, client should be trensferred to some default page and informed that error occured

## Keywords: ##
Java EE, Spring, PostgreSQL, SQL, HTML, CSS, JSP, Maven, Tomcat