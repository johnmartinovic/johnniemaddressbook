package com.johnniem.dbdaoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.model.City;
import com.johnniem.model.Contact;
import com.johnniem.model.Country;
import com.johnniem.utils.GeneralUtils;

public class DBDAOImpl implements DBDAO {
	
	private JdbcTemplate jdbcTemplate;
	 
    public DBDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    //================================================================================
    // DB initialization operations
    //================================================================================
    
    @Override
	public void createDB() {
		String sqlStr = "CREATE DATABASE AddressBook";
		
		jdbcTemplate.execute(sqlStr);
	}

	@Override
	public void createAllTables() {
		List<String> sqlStrList = new ArrayList<>();
		
		sqlStrList.add(
				" CREATE TABLE COUNTRY (" +
				"  ID 			BIGSERIAL 		PRIMARY KEY," +
				"  NAME 		VARCHAR(50) 	UNIQUE," +
				"  ALPHA_2 		VARCHAR(2) 		UNIQUE," +
				"  ALPHA_3 		VARCHAR(3) 		UNIQUE" +
				" )"
		);
		
		sqlStrList.add(
				" CREATE TABLE CITY (" +
				"  ID 			BIGSERIAL 		PRIMARY KEY," +
				"  NAME 		VARCHAR(50) 	UNIQUE," +
				"  ZIP_CODE 	VARCHAR(10)," +
				"  COUNTRY_ID 	BIGINT 			REFERENCES COUNTRY(ID) ON DELETE CASCADE ON UPDATE CASCADE" +
				" )"
		);
		
		sqlStrList.add(
				" CREATE TABLE ADDRESS (" +
				"  ID 			BIGSERIAL 		PRIMARY KEY," +
				"  STREET 		VARCHAR(50)," +
				"  STREET_NO 	VARCHAR(5)," +
				"  CITY_ID 		BIGINT 			REFERENCES CITY(ID) ON DELETE CASCADE ON UPDATE CASCADE" +
				" )"
		);
		
		sqlStrList.add(
				" CREATE TABLE SEX (" +
				"  ID 			BIGSERIAL 		PRIMARY KEY," +
				"  NAME 		VARCHAR(7) 		UNIQUE" +
				" )"
		);
		
		sqlStrList.add(
				" CREATE TABLE CONTACT (" +
				"  ID 			BIGSERIAL 		PRIMARY KEY," +
				"  FIRST_NAME 	VARCHAR(20)," +
				"  LAST_NAME 	VARCHAR(50)," +
				"  PHONE 		VARCHAR(13)," +
				"  EMAIL 		VARCHAR(50)," +
				"  SEX_ID 		BIGINT 			REFERENCES SEX(ID) ON DELETE CASCADE ON UPDATE CASCADE," +
				"  ADDRESS_ID 	BIGINT 			REFERENCES ADDRESS(ID) ON DELETE CASCADE ON UPDATE CASCADE" +
				" )"
		);
		
		for (String sqlStr : sqlStrList)
			jdbcTemplate.execute(sqlStr);
	}
	
	@Override
	public void createFunctions() {
		
		List<String> sqlStrList = new ArrayList<>();
		
		// function GET_COUNTRY_ID
		sqlStrList.add(
				" CREATE OR REPLACE FUNCTION GET_COUNTRY_ID(COUNTRY_NAME VARCHAR(50))" +
				"  RETURNS BIGINT AS $$" +
				" DECLARE ID_FOUND BIGINT;" +
				" BEGIN" +
				"  SELECT ID INTO ID_FOUND" +
				"  FROM COUNTRY" +
				"  WHERE NAME = COUNTRY_NAME;" +
				"  RETURN ID_FOUND;" +
				" END;" +
				" $$ LANGUAGE plpgsql;"
		);
		
		// function GET_CITY_ID
		sqlStrList.add(
				" CREATE OR REPLACE FUNCTION GET_CITY_ID(CITY_NAME VARCHAR(50))" +
				"  RETURNS BIGINT AS $$" +
				" DECLARE ID_FOUND BIGINT;" +
				" BEGIN" +
				"  SELECT ID INTO ID_FOUND" +
				"  FROM CITY" +
				"  WHERE NAME = CITY_NAME;" +
				"  RETURN ID_FOUND;" +
				" END;" +
				" $$ LANGUAGE plpgsql;"
		);
		
		// function GET_SEX_ID
		sqlStrList.add(
				" CREATE OR REPLACE FUNCTION GET_SEX_ID(SEX_NAME VARCHAR(7))" +
				"  RETURNS BIGINT AS $$" +
				" DECLARE ID_FOUND BIGINT;" +
				" BEGIN" +
				"  SELECT ID INTO ID_FOUND" +
				"  FROM SEX" +
				"  WHERE NAME = SEX_NAME;" +
				"  RETURN ID_FOUND;" +
				" END;" +
				" $$ LANGUAGE plpgsql;"
		);
		
		for (String sqlStr : sqlStrList)
			jdbcTemplate.execute(sqlStr);
	}
	
	@Override
	public void deleteAllTables() {
		List<String> sqlTableList = new ArrayList<>();
		sqlTableList.add("CONTACT");
		sqlTableList.add("SEX");
		sqlTableList.add("ADDRESS");
		sqlTableList.add("CITY");
		sqlTableList.add("COUNTRY");
		
		for (String sqlTable : sqlTableList) {
			try {
				jdbcTemplate.execute("DROP TABLE " + sqlTable);
			}
			catch (Exception se) {
				se.printStackTrace();;
			}
		}
	}
	
	@Override
	public void initTableSex() {
		// Insert sex values
        String sqlStr =
        		" INSERT INTO SEX (NAME)" +
                "  VALUES (?)";
        
        jdbcTemplate.update(sqlStr, "Male");
        jdbcTemplate.update(sqlStr, "Female");
	}
	
	@Override
	public void fillTablesWithValues() {

		// Insert country values
		List<Country> countryList = GeneralUtils.getCountriesFromCSV();
		
        String sqlStr =
        		" INSERT INTO COUNTRY (NAME, ALPHA_2, ALPHA_3)" +
                "  VALUES (?, ?, ?)";
        
        for (Country country : countryList) {
        	jdbcTemplate.update(sqlStr, country.getName(), country.getAlpha2(), country.getAlpha3());
        }

        
		// Insert city values
		Country usaCountry = new Country("United States", "US", "USA");
		Country hrvCountry = new Country("Croatia", "HR", "HRV");
		Country nldCountry = new Country("Netherlands", "NL", "NLD");
		
		List<City> cityList = new ArrayList<>();
		cityList.add(new City("New York", "21547", usaCountry));
		cityList.add(new City("LA", "48971", usaCountry));
		cityList.add(new City("Zagreb", "10000", hrvCountry));
		cityList.add(new City("Split", "58000", hrvCountry));
		cityList.add(new City("Amsterdam", "10000", nldCountry));
		
        sqlStr =
        		" INSERT INTO CITY (NAME, ZIP_CODE, COUNTRY_ID)" +
                "  VALUES (?, ?, GET_COUNTRY_ID(?))";
        
        for (City city : cityList) {
        	jdbcTemplate.update(sqlStr, city.getName(), city.getZipCode(), city.getCountry().getName());
        }
		
        
        // Insert contact values
	    List<Contact> contactsList = new ArrayList<>();
	    contactsList.add(new Contact("Josip", "Pavkovic", "091555551", "jozo@gmail.com", "Male", "Tu negdje", "89516", cityList.get(2)));
	    contactsList.add(new Contact("Anka", "Mrak", "0915786421", "anka@yahoo.com", "Female", "Left st.", "13541", cityList.get(0)));
	    contactsList.add(new Contact("Mirko", "Bleblu", "095321698", "mirkobleblu@bleblu.com", "Male", "7th avenue", "3215", cityList.get(1)));
	    contactsList.add(new Contact("Janko", "James", "048974516", "jankec@cool.com", "Male", "Right st.", "101", cityList.get(2)));
	    
	    String sqlStrAddress =
        		" INSERT INTO ADDRESS (STREET, STREET_NO, CITY_ID)" +
                "  VALUES (?, ?, GET_CITY_ID(?))";
	    
	    String sqlStrContact =
        		" INSERT INTO CONTACT (FIRST_NAME, LAST_NAME, PHONE, EMAIL, SEX_ID, ADDRESS_ID)" +
                "  VALUES (?, ?, ?, ?, GET_SEX_ID(?), ?)";
	    
	    GeneratedKeyHolder holder = new GeneratedKeyHolder();
	    long primaryKey;
	    for (Contact contact : contactsList) {
	    	jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sqlStrAddress, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, contact.getStreet());
                    ps.setString(2, contact.getStreetNo());
                    ps.setString(3, contact.getCity().getName());
                    return ps;
				}
	    	}, holder);
	    	
	    	primaryKey = (Long) holder.getKeys().get("Id");
	    	
	    	jdbcTemplate.update(sqlStrContact, contact.getFirstName(), contact.getLastName(),
	    			contact.getPhone(), contact.getEmail(), contact.getSex(), primaryKey);
	    }
	}
	
	@Override
	public List<String> listSex() {
		String sqlStr =
				" SELECT NAME" +
				" FROM SEX";
		
	    List<String> listSex = jdbcTemplate.queryForList(sqlStr, String.class);
	 
	    return listSex;
	}

    //================================================================================
    // Country operations
    //================================================================================
	
	@Override
	public void updateCountry(Country country) {
		if (country.getId() > 0) {
	        // Update country
			String sqlStr = 
					" UPDATE COUNTRY SET" +
					"  NAME = ?," +
					"  ALPHA_2 = ?," +
					"  ALPHA_3 = ?" +
					" WHERE" +
					"  ID = ?";
			
	        jdbcTemplate.update(sqlStr, country.getName(), country.getAlpha2(),
	        		country.getAlpha3(), country.getId());
	    } else {
	        // Insert new country
	        String sqlStr =
	        		" INSERT INTO COUNTRY (NAME, ALPHA_2, ALPHA_3)" +
	                "  VALUES (?, ?, ?)";
	        
	        jdbcTemplate.update(sqlStr, country.getName(), country.getAlpha2(),
	        		country.getAlpha3());
	    }
	}

	@Override
	public void deleteCountry(int countryId) {
		String sqlStr =
				" DELETE FROM COUNTRY" +
				" WHERE" +
				"  ID = ?";
		
	    jdbcTemplate.update(sqlStr, countryId);
	}
	
	@Override
	public boolean existsCountryId(int id) {
		String sqlStr =
				" SELECT COUNT(*)" +
				" FROM COUNTRY" +
				" WHERE ID = ?" +
				" LIMIT 1";
		
		Integer count = jdbcTemplate.queryForObject(sqlStr, Integer.class, id);
		
		return (count != null && count > 0);
	}
	
	@Override
	public boolean existsCountryName(String countryName) {
		String sqlStr =
				" SELECT COUNT(*)" +
				" FROM COUNTRY" +
				" WHERE NAME = ?" +
				" LIMIT 1";
		
		Integer count = jdbcTemplate.queryForObject(sqlStr, Integer.class, countryName);
		
		return (count != null && count > 0);
	}

	@Override
	public boolean existsCountryAlpha2(String countryAlpha2) {
		String sqlStr =
				" SELECT COUNT(*)" +
				" FROM COUNTRY" +
				" WHERE ALPHA_2 = ?" +
				" LIMIT 1";
		
		Integer count = jdbcTemplate.queryForObject(sqlStr, Integer.class, countryAlpha2);
		
		return (count != null && count > 0);
	}

	@Override
	public boolean existsCountryAlpha3(String countryAlpha3) {
		String sqlStr =
				" SELECT COUNT(*)" +
				" FROM COUNTRY" +
				" WHERE ALPHA_3 = ?" +
				" LIMIT 1";
		
		Integer count = jdbcTemplate.queryForObject(sqlStr, Integer.class, countryAlpha3);
		
		return (count != null && count > 0);
	}

	@Override
	public Country getCountry(int countryId) {
		 String sqlStr =
				 " SELECT *" +
			     " FROM COUNTRY" +
				 " WHERE" +
				 "  ID = ?";
		 
		 return jdbcTemplate.query(sqlStr, new CountryResultSetExtractor(), countryId);
	}
	
	@Override
	public Country getCountry(String countryName) {
		 String sqlStr =
				 " SELECT *" +
			     " FROM COUNTRY" +
				 " WHERE" +
				 "  NAME = ?";
		 
		 return jdbcTemplate.query(sqlStr, new CountryResultSetExtractor(), countryName);
	}

	@Override
	public List<Country> listCountry() {
		String sqlStr =
				" SELECT *" +
				" FROM COUNTRY";
		
	    List<Country> listCountry = jdbcTemplate.query(sqlStr, new CountryRowMapper());
	 
	    return listCountry;
	}
	
	public class CountryRowMapper implements RowMapper<Country> {

		public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
			Country country = new Country();
			
			country.setId(rs.getInt("ID"));
			country.setName(rs.getString("NAME"));
			country.setAlpha2(rs.getString("ALPHA_2"));
			country.setAlpha3(rs.getString("ALPHA_3"));
			
			return country;
		}
	}
	
	public class CountryResultSetExtractor implements ResultSetExtractor<Country> {

		public Country extractData(ResultSet rs) throws SQLException, DataAccessException {
			Country country = null;
			
			if (rs.next()) {
				country = new Country();

				country.setId(rs.getInt("ID"));
				country.setName(rs.getString("NAME"));
				country.setAlpha2(rs.getString("ALPHA_2"));
				country.setAlpha3(rs.getString("ALPHA_3"));
			}
			
			return country;
		}
	}

    //================================================================================
    // City operations
    //================================================================================

//	@Override
//	public void updateCity(City city) {
//		if (city.getId() > 0) {
//	        // Update city
//			String sqlStr = 
//					" UPDATE CITY SET" +
//					"  NAME = ?," +
//					"  ZIP_CODE = ?," +
//					"  COUNTRY_ID = ?" +
//					" WHERE" +
//					"  ID = ?";
//			
//	        jdbcTemplate.update(sqlStr, city.getName(), city.getZipCode(), city.getCountry().getId());
//	    } else {
//	        // Insert new city
//	        String sqlStr =
//	        		" INSERT INTO CITY (NAME, ZIP_CODE, COUNTRY_ID)" +
//	                "  VALUES (?, ?, ?)";
//	        
//	        jdbcTemplate.update(sqlStr, city.getName(), city.getCountry().getId());
//	    }
//	}
	
	@Override
	public void updateCity(City city) {
		if (city.getId() > 0) {
	        // Update city
			String sqlStr = 
					" UPDATE CITY SET" +
					"  NAME = ?," +
					"  ZIP_CODE = ?," +
					"  COUNTRY_ID = ?" +
					" WHERE" +
					"  ID = ?";
			
	        jdbcTemplate.update(sqlStr, city.getName(), city.getZipCode(), city.getCountryId(), city.getId());
	    } else {
	        // Insert new city
	        String sqlStr =
	        		" INSERT INTO CITY (NAME, ZIP_CODE, COUNTRY_ID)" +
	                "  VALUES (?, ?, ?)";
	        
	        jdbcTemplate.update(sqlStr, city.getName(), city.getZipCode(), city.getCountryId());
	    }
	}

	@Override
	public void deleteCity(int cityId) {
		String sqlStr =
				" DELETE FROM CITY" +
				" WHERE" +
				"  ID = ?";
		
	    jdbcTemplate.update(sqlStr, cityId);
	}
	
	@Override
	public boolean existsCityName(String cityName) {
		String sqlStr =
				" SELECT COUNT(*)" +
				" FROM CITY" +
				" WHERE NAME = ?" +
				" LIMIT 1";
		
		Integer count = jdbcTemplate.queryForObject(sqlStr, Integer.class, cityName);
		
		return (count != null && count > 0);
	}

	@Override
	public City getCity(int cityId) {
		String sqlStr =
				" SELECT CITY.ID, CITY.NAME, CITY.ZIP_CODE, COUNTRY.ID, COUNTRY.NAME, COUNTRY.ALPHA_2, COUNTRY.ALPHA_3" +
			    " FROM CITY" +
			    "  INNER JOIN COUNTRY ON CITY.COUNTRY_ID = COUNTRY.ID" +
				" WHERE" +
				"  CITY.ID = ?";
		
		return jdbcTemplate.query(sqlStr, new CityResultSetExtractor(), cityId);
	}

	@Override
	public List<City> listCity() {
		String sqlStr =
				" SELECT CITY.ID, CITY.NAME, CITY.ZIP_CODE, COUNTRY.ID, COUNTRY.NAME, COUNTRY.ALPHA_2, COUNTRY.ALPHA_3" +
			    " FROM CITY" +
			    "  INNER JOIN COUNTRY ON CITY.COUNTRY_ID = COUNTRY.ID";
		
		List<City> listCity = jdbcTemplate.query(sqlStr, new CityRowMapper());
	 
	    return listCity;
	}
	
	public class CityRowMapper implements RowMapper<City> {

		public City mapRow(ResultSet rs, int rowNum) throws SQLException {
			Country country = new Country();
			country.setId(rs.getInt(4));
			country.setName(rs.getString(5));
			country.setAlpha2(rs.getString(6));
			country.setAlpha3(rs.getString(7));
			
			City city = new City();
			city.setId(rs.getInt(1));
			city.setName(rs.getString(2));
			city.setZipCode(rs.getString(3));
			city.setCountry(country);
			
			return city;
		}
	}
	
	public class CityResultSetExtractor implements ResultSetExtractor<City> {

		public City extractData(ResultSet rs) throws SQLException, DataAccessException {
			City city = null;
			
			if (rs.next()) {
				Country country = new Country();
				country.setId(rs.getInt(4));
				country.setName(rs.getString(5));
				country.setAlpha2(rs.getString(6));
				country.setAlpha3(rs.getString(7));
				
				city = new City();
				city.setId(rs.getInt(1));
				city.setName(rs.getString(2));
				city.setZipCode(rs.getString(3));
				city.setCountry(country);
			}
			
			return city;
		}
	}
	
	
	//================================================================================
    // Contact operations
    //================================================================================
	
	@Override
	public void updateContact(Contact contact) {
		// TODO ?
		// address is always newly inserted
	    GeneratedKeyHolder holder = new GeneratedKeyHolder();
	    long addressKey;
	    
    	String sqlStrAddress =
        		" INSERT INTO ADDRESS (STREET, STREET_NO, CITY_ID)" +
                "  VALUES (?, ?, ?)";
    	
    	jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sqlStrAddress, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, contact.getStreet());
                ps.setString(2, contact.getStreetNo());
                ps.setInt(3, contact.getCityId());
                return ps;
			}
    	}, holder);
    	addressKey = (Long) holder.getKeys().get("Id");
	    
		if (contact.getId() > 0) {
	        // Update contact
			String sqlStr = 
					" UPDATE CONTACT SET" +
					"  FIRST_NAME = ?," +
					"  LAST_NAME = ?," +
					"  PHONE = ?," +
					"  EMAIL = ?," +
					"  SEX_ID = GET_SEX_ID(?)," +
					"  ADDRESS_ID = ?" +
					" WHERE" +
					"  ID = ?";
			
	        jdbcTemplate.update(sqlStr, contact.getFirstName(), contact.getLastName(), contact.getPhone(), contact.getEmail(),
	        		contact.getSex(), addressKey, contact.getId());
	    } else {
	    	// Insert new contact
		    String sqlStrContact =
	        		" INSERT INTO CONTACT (FIRST_NAME, LAST_NAME, PHONE, EMAIL, SEX_ID, ADDRESS_ID)" +
	                "  VALUES (?, ?, ?, ?, GET_SEX_ID(?), ?)";
		    
	    	jdbcTemplate.update(sqlStrContact, contact.getFirstName(), contact.getLastName(),
	    			contact.getPhone(), contact.getEmail(), contact.getSex(), addressKey);
	    }
	}

	@Override
	public void deleteContact(int contactId) {
		String sqlStr =
				" DELETE FROM CONTACT" +
				" WHERE" +
				"  ID = ?";
		
	    jdbcTemplate.update(sqlStr, contactId);
	}
	
	@Override
	public boolean existsAddress(String street, String streetNo, String cityName) {
		String sqlStr =
				" SELECT COUNT(*)" +
				" FROM ADDRESS" +
				"  INNER JOIN CITY ON CITY_ID = CITY.ID" +
				" WHERE" +
				"  ADDRESS.STREET = ? AND ADDRESS.STREET_NO = ? AND CITY.NAME=?" +
				" LIMIT 1";
		
		Integer count = jdbcTemplate.queryForObject(sqlStr, Integer.class, street, streetNo, cityName);
		
		return (count != null && count > 0);
	}
	
	@Override
	public Contact getContact(int contactId) {
		String sqlStr =
				" SELECT CONTACT.ID, CONTACT.FIRST_NAME, CONTACT.LAST_NAME, CONTACT.PHONE, CONTACT.EMAIL, SEX.ID, SEX.NAME," +
				"  ADDRESS.ID, ADDRESS.STREET, ADDRESS.STREET_NO, CITY.ID, CITY.NAME, CITY.ZIP_CODE," +
				"  COUNTRY.ID, COUNTRY.NAME, COUNTRY.ALPHA_2, COUNTRY.ALPHA_3" +
			    " FROM CONTACT" +
			    "  INNER JOIN SEX ON CONTACT.SEX_ID = SEX.ID" +
			    "  INNER JOIN ADDRESS ON CONTACT.ADDRESS_ID = ADDRESS.ID" +
			    "  INNER JOIN CITY ON ADDRESS.CITY_ID = CITY.ID" +
			    "  INNER JOIN COUNTRY ON CITY.COUNTRY_ID = COUNTRY.ID" +
				" WHERE" +
				"  CONTACT.ID = ?";
		
		return jdbcTemplate.query(sqlStr, new ContactResultSetExtractor(), contactId);
	}

	@Override
	public List<Contact> listContact() {
		String sqlStr =
				" SELECT CONTACT.ID, CONTACT.FIRST_NAME, CONTACT.LAST_NAME, CONTACT.PHONE, CONTACT.EMAIL, SEX.ID, SEX.NAME," +
				"  ADDRESS.ID, ADDRESS.STREET, ADDRESS.STREET_NO, CITY.ID, CITY.NAME, CITY.ZIP_CODE," +
				"  COUNTRY.ID, COUNTRY.NAME, COUNTRY.ALPHA_2, COUNTRY.ALPHA_3" +
			    " FROM CONTACT" +
			    "  INNER JOIN SEX ON CONTACT.SEX_ID = SEX.ID" +
			    "  INNER JOIN ADDRESS ON CONTACT.ADDRESS_ID = ADDRESS.ID" +
			    "  INNER JOIN CITY ON ADDRESS.CITY_ID = CITY.ID" +
			    "  INNER JOIN COUNTRY ON CITY.COUNTRY_ID = COUNTRY.ID";
		
		List<Contact> listContact = jdbcTemplate.query(sqlStr, new ContactRowMapper());
	 
	    return listContact;
	}
	
	public class ContactRowMapper implements RowMapper<Contact> {

		public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
			Country country = new Country();
			country.setId(rs.getInt(14));
			country.setName(rs.getString(15));
			country.setAlpha2(rs.getString(16));
			country.setAlpha3(rs.getString(17));
			
			City city = new City();
			city.setId(rs.getInt(11));
			city.setName(rs.getString(12));
			city.setZipCode(rs.getString(13));
			city.setCountry(country);
			
			Contact contact = new Contact();
			contact.setId(rs.getInt(1));
			contact.setFirstName(rs.getString(2));
			contact.setLastName(rs.getString(3));
			contact.setPhone(rs.getString(4));
			contact.setEmail(rs.getString(5));
			contact.setSexId(rs.getInt(6));
			contact.setSex(rs.getString(7));
			contact.setAddressId(rs.getInt(8));
			contact.setStreet(rs.getString(9));
			contact.setStreetNo(rs.getString(10));
			contact.setCity(city);
			
			return contact;
		}
	}
	
	public class ContactResultSetExtractor implements ResultSetExtractor<Contact> {

		public Contact extractData(ResultSet rs) throws SQLException, DataAccessException {
			Contact contact = null;
			
			if (rs.next()) {
				Country country = new Country();
				country.setId(rs.getInt(14));
				country.setName(rs.getString(15));
				country.setAlpha2(rs.getString(16));
				country.setAlpha3(rs.getString(17));
				
				City city = new City();
				city.setId(rs.getInt(11));
				city.setName(rs.getString(12));
				city.setZipCode(rs.getString(13));
				city.setCountry(country);
				
				contact = new Contact();
				contact.setId(rs.getInt(1));
				contact.setFirstName(rs.getString(2));
				contact.setLastName(rs.getString(3));
				contact.setPhone(rs.getString(4));
				contact.setEmail(rs.getString(5));
				contact.setSexId(rs.getInt(6));
				contact.setSex(rs.getString(7));
				contact.setAddressId(rs.getInt(8));
				contact.setStreet(rs.getString(9));
				contact.setStreetNo(rs.getString(10));
				contact.setCity(city);
			}
			
			return contact;
		}
	}
}
