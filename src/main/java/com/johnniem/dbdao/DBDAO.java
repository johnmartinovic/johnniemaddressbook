package com.johnniem.dbdao;

import java.util.List;

import com.johnniem.model.City;
import com.johnniem.model.Contact;
import com.johnniem.model.Country;

public interface DBDAO {
	
	// methods for DB initialization
	public void createDB();
	
	public void createAllTables();
	
	public void createFunctions();
	
	public void deleteAllTables();
	
	public void initTableSex();
	
	public void fillTablesWithValues();
	
	
	// list all country data
	public List<String> listSex();
    
    // create new or update specified country
 	public void updateCountry(Country country);
 	
 	// delete country
 	public void deleteCountry(int countryId);
 	
 	// get country info
 	public Country getCountry(int countryId);
 	
 	public Country getCountry(String countryName);
 	
	// list all country data
	public List<Country> listCountry();
	
	// check if country id exists
	public boolean existsCountryId(int id);
	
	// check if country name exists
	public boolean existsCountryName(String countryName);
	
	// check if country Alpha-2 exists
	public boolean existsCountryAlpha2(String countryAlpha2);
	
	// check if country Alpha-3 exists
	public boolean existsCountryAlpha3(String countryAlpha3);
    
	
	
    // create new or update specified city
 	public void updateCity(City city);
 	
 	// delete city
 	public void deleteCity(int cityId);
 	
 	// get city info
 	public City getCity(int cityId);
 	
	// list all city data
	public List<City> listCity();
	
	// check if city name exists
	public boolean existsCityName(String cityName);
	
	
	
	// create new or update specified contact
	public void updateContact(Contact contact);
    
	// delete contact
    public void deleteContact(int contactId);
    
    // get contact info
    public Contact getContact(int contactId);
    
    // list all contacts data
    public List<Contact> listContact();
    
    // check if address already exist
    public boolean existsAddress(String street, String streetNo, String cityName);
}
