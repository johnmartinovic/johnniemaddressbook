package com.johnniem.model;

import java.util.Comparator;

public class Contact implements Comparable<Contact> {

	private int id;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String sex;
	private int sexId;
	private String street;
	private String streetNo;
	private int addressId;
	private int cityId;
	private int countryId;
	private City city;
	
	public Contact() {
	}

	public Contact(String firstName, String lastName, String phone, String email, String sex, String street,
			String streetNo, City city) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.sex = sex;
		this.street = street;
		this.streetNo = streetNo;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getSexId() {
		return sexId;
	}

	public void setSexId(int sexId) {
		this.sexId = sexId;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	public static Comparator<Contact> ContactNameComparator
	= new Comparator<Contact>() {

		@Override
		public int compare(Contact o1, Contact o2) {
			return o1.getFirstName().compareTo(o2.getFirstName());
		}
	};

	@Override
	public int compareTo(Contact o1) {
		return getFirstName().compareTo(o1.getFirstName());
	}
	
	@Override
	public String toString() {
		return getFirstName() + ", " + getLastName();
	}
}