package com.johnniem.model;

import java.util.Comparator;

public class City implements Comparable<City> {
	
	private int id;
	private String name;
	private String zipCode;
	private Country country;
	private int countryId;
	
	public City() {
	}
	
	public City(String name, String zipCode, Country country) {
		this.name = name;
		this.zipCode = zipCode;
		this.country = country;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public Country getCountry() {
		return country;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public static Comparator<City> CityNameComparator
		= new Comparator<City>() {

			@Override
			public int compare(City o1, City o2) {
				return o1.getName().compareTo(o2.getName());
			}
	};

	@Override
	public int compareTo(City o1) {
		return getName().compareTo(o1.getName());
	}

	@Override
	public String toString() {
		return getName() + ", " + getZipCode() + ", " + getCountry();
	}
}
