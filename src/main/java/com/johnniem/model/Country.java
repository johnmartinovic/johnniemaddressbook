package com.johnniem.model;

import java.util.Comparator;

public class Country implements Comparable<Country> {
	
	private int id;
	private String name;
	private String alpha2;
	private String alpha3;
	
	public Country() {
	}
	
	public Country(String name, String alpha2, String alpha3) {
		this.name = name;
		this.alpha2 = alpha2;
		this.alpha3 = alpha3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAlpha2() {
		return alpha2;
	}
	
	public void setAlpha2(String alpha2) {
		this.alpha2 = alpha2;
	}
	
	public String getAlpha3() {
		return alpha3;
	}
	
	public void setAlpha3(String alpha3) {
		this.alpha3 = alpha3;
	}
	
	public static Comparator<Country> CountryNameComparator
		= new Comparator<Country>() {

			@Override
			public int compare(Country o1, Country o2) {
				return o1.getName().compareTo(o2.getName());
			}
	};

	@Override
	public int compareTo(Country o1) {
		return getName().compareTo(o1.getName());
	}
	
	@Override
	public String toString() {
		return getName() + ", " + getAlpha2() + ", " + getAlpha3();
	}
	
}
