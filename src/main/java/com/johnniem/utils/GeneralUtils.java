package com.johnniem.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import com.johnniem.model.Country;
import com.opencsv.CSVReader;

/**
* <h1>General utilities</h1>
* 
* @author  JohnnieM
*/
public class GeneralUtils {
	
	// location of a CSV file with countres
	public static final String csvLocation = "/countryData/wikipedia-iso-country-codes.csv";
	
	// parse CSV and get list of countries
	public static List<Country> getCountriesFromCSV() {
		List<Country> countries = new LinkedList<Country>();
		
		try {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(csvLocation);
			
			CSVReader reader = new CSVReader(new InputStreamReader(inputStream));

			String [] nextLine;
			// skip first line - column names are defined here
			reader.readNext();
			
			while ((nextLine = reader.readNext()) != null) {
				countries.add(
						new Country(nextLine[0],nextLine[1],nextLine[2])
				);
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return countries;
	}
}
