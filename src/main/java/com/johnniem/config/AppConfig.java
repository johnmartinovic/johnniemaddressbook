package com.johnniem.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.dbdaoimpl.DBDAOImpl;

/**
* <h1>Basic Spring Configuration</h1>
* 
* @author  JohnnieM
*/
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.johnniem"})
public class AppConfig extends WebMvcConfigurerAdapter {
	/**
    * Define views prefixes and suffixes
    */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");

		return viewResolver;
	}
	
	/**
    * Define DB connection parameters
    */
	@Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/addressbook");
        dataSource.setUsername("johnm");
        dataSource.setPassword("sP49P8a3#2");
    	
        return dataSource;
    }
	
	/**
    * Get DB access object
    */
	@Bean
    public DBDAO getDBDAO() {
        return new DBDAOImpl(getDataSource());
    }
	
	/**
    * Access resource bundles
    */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource rb = new ResourceBundleMessageSource();
		rb.setBasenames(new String[] { "messages" });
		return rb;
	}
	
	/**
    * Serve static resources
    */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

}