package com.johnniem.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.johnniem.model.Contact;

/**
* <h1>Validator class for Contact objects</h1>
* 
* @author  JohnnieM
*/
@Component
public class ContactFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return Contact.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.contactForm.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.contactForm.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty.contactForm.phone");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.contactForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sex", "NotEmpty.contactForm.sex");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "street", "NotEmpty.contactForm.street");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "streetNo", "NotEmpty.contactForm.streetNo");
		
		
	}

}
