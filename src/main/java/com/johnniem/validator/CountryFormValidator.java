package com.johnniem.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.model.Country;

/**
* <h1>Validator class for City objects</h1>
* 
* @author  JohnnieM
*/
@Component
public class CountryFormValidator implements Validator {
	
	@Autowired
    private DBDAO dbDAO;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Country.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Country country = (Country) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.countryForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "alpha2", "NotEmpty.countryForm.alpha2");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "alpha3", "NotEmpty.countryForm.alpha3");
		
		// checks done only for new countries
		if (country.getId() == 0) {
			if (dbDAO.existsCountryName(country.getName()))
				errors.rejectValue("name", "Valid.countryForm.countryExists");
			
			if (dbDAO.existsCountryAlpha2(country.getAlpha2()))
				errors.rejectValue("alpha2", "Valid.countryForm.alpha2Exists");
			
			if (dbDAO.existsCountryAlpha3(country.getAlpha3()))
				errors.rejectValue("alpha3", "Valid.countryForm.alpha3Exists");
		}
		
		if (country.getAlpha2().length() != 2)
			errors.rejectValue("alpha2", "Valid.countryForm.lengthAlpha2");
		
		if (country.getAlpha3().length() != 3)
			errors.rejectValue("alpha3", "Valid.countryForm.lengthAlpha3");
	}

}
