package com.johnniem.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.model.City;

/**
* <h1>Validator class for City objects</h1>
* 
* @author  JohnnieM
*/
@Component
public class CityFormValidator implements Validator {
	
	@Autowired
    private DBDAO dbDAO;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return City.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		City city = (City) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.cityForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipCode", "NotEmpty.cityForm.zipCode");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "countryId", "NotEmpty.cityForm.countryId");
		
		if (!dbDAO.existsCountryId(city.getCountryId()))
			errors.rejectValue("name", "Valid.cityForm.countryDoesntExist");
		
		// checks done only for new cities
		if (city.getId() == 0) {
			if (dbDAO.existsCityName(city.getName()))
				errors.rejectValue("name", "Valid.cityForm.cityExists");
		}
	}

}
