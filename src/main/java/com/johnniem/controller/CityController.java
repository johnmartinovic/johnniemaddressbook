package com.johnniem.controller;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.model.City;
import com.johnniem.model.Country;
import com.johnniem.validator.CityFormValidator;

/**
* <h1>Controller for City ops based mappings</h1>
* 
* @author  JohnnieM
*/
@Controller
@RequestMapping("/cities")
public class CityController {
	
	@Autowired
    private DBDAO dbDAO;
	
	@Autowired
    private CityFormValidator cityFormValidator;
	
	/**
    * Get city list and present it to client
    */
	@GetMapping
	public String showCityList(Model model) {
		List<City> cityList = dbDAO.listCity();
		
		Collections.sort(cityList, City.CityNameComparator);
		
		model.addAttribute("cityList", cityList);
	 
	    return "cityList";
	}
	
	/**
    * Process submitted City value
    */
	@PostMapping
	public String processAddCity(
			@ModelAttribute("cityForm") final City city,
			final BindingResult result,
			final RedirectAttributes redirectAttributes) {
		boolean cityModified = false;

		// store attributes through redirect operation
    	redirectAttributes.addFlashAttribute("cityForm", city);
    	
		//Check validation errors
		cityFormValidator.validate(city, result);
	    if (result.hasErrors()) {
	    	// inform user about wrong values
	    	redirectAttributes.addFlashAttribute("cityModified", cityModified);
	    	// store validation result through redirect operation
	        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.cityForm", result);
	        return "redirect:/cities/add";
	    }
	    
	    // Store city info to DB
		dbDAO.updateCity(city);
	    
		// store cityModified flag to inform the user
		cityModified = true;
	    redirectAttributes.addFlashAttribute("cityModified", cityModified);
	    if (city.getId() == 0)
	    	redirectAttributes.addFlashAttribute("cityModifiedMsg", city.getName() + " successfully added!");
	    else
	    	redirectAttributes.addFlashAttribute("cityModifiedMsg", "City successfully edited!");

	    return "redirect:/cities";
	}
	
	/**
    * Open City add window
    */
	@GetMapping("/add")
	public String showAddCity(Model model) {
		if(!model.containsAttribute("cityForm"))
			model.addAttribute("cityForm", new City());
		
	    model.addAttribute("countryMap", getCountryMapForm());
	    
	    return "cityEdit";
	}
	
	/**
    * Open City add window and fill it with existing data
    */
	@GetMapping("/{cityId}/update")
	public String showUpdateCity(
			@PathVariable("cityId") int cityId,
			Model model) {
		City city = dbDAO.getCity(cityId);
		
	    model.addAttribute("cityForm", city);
	    
	    model.addAttribute("countryMap", getCountryMapForm());
	    
	    return "cityEdit";
	}
	
	/**
    * Delete City add window and fill it with existing data
    */
	@GetMapping("/{cityId}/delete")
	public String deleteCity(
			@PathVariable("cityId") int cityId,
			final RedirectAttributes redirectAttributes) {
		City city = dbDAO.getCity(cityId);
		
	    dbDAO.deleteCity(cityId);

	    boolean cityModified = true;
	    redirectAttributes.addFlashAttribute("cityModified", cityModified);
	    redirectAttributes.addFlashAttribute("cityModifiedMsg", city.getName() + " successfully deleted!");
	    
	    return "redirect:/cities";
	}
	
	/**
    * Get country list, order it and return it as a Map
    */
	public Map<Integer,String> getCountryMapForm(){
		List<Country> countryList = dbDAO.listCountry();
		Collections.sort(countryList, Country.CountryNameComparator);
	    Map<Integer,String> countryMap = new LinkedHashMap<>();
	    for (Country country : countryList) {
		    countryMap.put(country.getId(), country.getName());
	    }
	    
	    return countryMap;
	}
}
