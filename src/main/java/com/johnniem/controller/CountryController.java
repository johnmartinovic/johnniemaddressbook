package com.johnniem.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.model.Country;
import com.johnniem.validator.CountryFormValidator;

/**
* <h1>Controller for Countries ops based mappings</h1>
* 
* @author  JohnnieM
*/
@Controller
@RequestMapping("/countries")
public class CountryController {
	
	@Autowired
    private DBDAO dbDAO;
	
	@Autowired
    private CountryFormValidator countryFormValidator;
	
	/**
    * Get country list and present it to client
    */
	@GetMapping
	public String showCountryList(Model model) {
		List<Country> countryList = dbDAO.listCountry();
		
		Collections.sort(countryList, Country.CountryNameComparator);
		
		model.addAttribute("countryList", countryList);
	 
	    return "countryList";
	}
	
	/**
    * Process submitted Country value
    */
	@PostMapping
	public String processAddCountry(
			@ModelAttribute("countryForm") final Country country,
			final BindingResult result,
			final RedirectAttributes redirectAttributes) {
		boolean countryModified = false;
		
		//Validation code
		countryFormValidator.validate(country, result);
		
    	redirectAttributes.addFlashAttribute("countryForm", country);
    	
		//Check validation errors
	    if (result.hasErrors()) {
	    	redirectAttributes.addFlashAttribute("countryModified", countryModified);
	        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.countryForm", result);
	        return "redirect:/countries/add";
	    }
	    
	    // Store country info to DB
		dbDAO.updateCountry(country);
	    
	    countryModified = true;
	    redirectAttributes.addFlashAttribute("countryModified", countryModified);
	    if (country.getId() == 0)
	    	redirectAttributes.addFlashAttribute("countryModifiedMsg", country.getName() + " successfully added!");
	    else
	    	redirectAttributes.addFlashAttribute("countryModifiedMsg", "Country successfully edited!");

	    return "redirect:/countries";
	}
	
	/**
    * Open Country add window
    */
	@GetMapping("/add")
	public String showAddCountry(Model model) {
		if(!model.containsAttribute("countryForm"))
			model.addAttribute("countryForm", new Country());
	    
	    return "countryEdit";
	}
	
	/**
    * Open Country add window and fill it with existing data
    */
	@GetMapping("/{countryId}/update")
	public String showUpdateCountry(
			@PathVariable("countryId") int countryId,
			Model model) {
		Country country = dbDAO.getCountry(countryId);
	    model.addAttribute("countryForm", country);
	    
	    return "countryEdit";
	}

	/**
    * Delete Country add window and fill it with existing data
    */
	@GetMapping("/{countryId}/delete")
	public String deleteCountry(
			@PathVariable("countryId") int countryId,
			final RedirectAttributes redirectAttributes) {
		Country country = dbDAO.getCountry(countryId);
		
	    dbDAO.deleteCountry(countryId);

	    boolean countryModified = true;
	    redirectAttributes.addFlashAttribute("countryModified", countryModified);
	    redirectAttributes.addFlashAttribute("countryModifiedMsg", country.getName() + " successfully deleted!");
	    
	    return "redirect:/countries";
	}
}
