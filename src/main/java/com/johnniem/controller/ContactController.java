package com.johnniem.controller;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.johnniem.dbdao.DBDAO;
import com.johnniem.model.City;
import com.johnniem.model.Contact;
import com.johnniem.validator.ContactFormValidator;

/**
* <h1>Controller for Contacts ops based mappings</h1>
* 
* @author  JohnnieM
*/
@Controller
@RequestMapping("/contacts")
public class ContactController {
	
	@Autowired
    private DBDAO dbDAO;
	
	@Autowired
    private ContactFormValidator contactFormValidator;
	
	/**
    * Get contact list and present it to client
    */
	@GetMapping
	public String showContactList(Model model) {
		List<Contact> contactList = dbDAO.listContact();
		
		Collections.sort(contactList, Contact.ContactNameComparator);
		
		model.addAttribute("contactList", contactList);
	 
	    return "contactList";
	}
	
	/**
    * Process submitted Contact value
    */
	@PostMapping
	public String processAddContact(
			@ModelAttribute("contactForm") final Contact contact,
			final BindingResult result,
			final RedirectAttributes redirectAttributes) {
		boolean contactModified = false;
		
		//Validation code
		contactFormValidator.validate(contact, result);
		
    	redirectAttributes.addFlashAttribute("contactForm", contact);
    	
		//Check validation errors
	    if (result.hasErrors()) {
	    	redirectAttributes.addFlashAttribute("contactModified", contactModified);
	        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.contactForm", result);
	        return "redirect:/contacts/add";
	    }
	    
	    // Store contact info to DB
		dbDAO.updateContact(contact);
	    
		contactModified = true;
	    redirectAttributes.addFlashAttribute("contactModified", contactModified);
	    if (contact.getId() == 0)
	    	redirectAttributes.addFlashAttribute("contactModifiedMsg", contact.getFirstName() + " " + contact.getLastName() + " successfully added!");
	    else
	    	redirectAttributes.addFlashAttribute("contactModifiedMsg", "Contact successfully edited!");

	    return "redirect:/contacts";
	}
	
	/**
    * Open Contact add window
    */
	@GetMapping("/add")
	public String showAddContact(Model model) {
		if(!model.containsAttribute("contactForm"))
			model.addAttribute("contactForm", new Contact());
		
	    model.addAttribute("cityMap", getCityMapForm());
	    model.addAttribute("sexList", dbDAO.listSex());
	    
	    return "contactEdit";
	}
	
	/**
    * Open Contact add window and fill it with existing data
    */
	@GetMapping("/{contactId}/update")
	public String showUpdateContact(
			@PathVariable("contactId") int contactId,
			Model model) {
		Contact contact = dbDAO.getContact(contactId);
		
	    model.addAttribute("contactForm", contact);
	    model.addAttribute("cityMap", getCityMapForm());
	    model.addAttribute("sexList", dbDAO.listSex());
	    
	    return "contactEdit";
	}
	
	/**
    * Delete City add window and fill it with existing data
    */
	@GetMapping("/{contactId}/delete")
	public String deleteContact(
			@PathVariable("contactId") int contactId,
			final RedirectAttributes redirectAttributes) {
		Contact contact = dbDAO.getContact(contactId);
		
	    dbDAO.deleteContact(contactId);

	    boolean contactModified = true;
	    redirectAttributes.addFlashAttribute("contactModified", contactModified);
	    redirectAttributes.addFlashAttribute("contactModifiedMsg", contact.getFirstName() + " " + contact.getLastName() + " successfully deleted!");
	    
	    return "redirect:/contacts";
	}
	
	/**
    * Get city list, order it and return it as a Map
    */
	public Map<Integer,String> getCityMapForm(){
		List<City> cityList = dbDAO.listCity();
		Collections.sort(cityList, City.CityNameComparator);
	    Map<Integer,String> cityMap = new LinkedHashMap<>();
	    for (City city : cityList) {
	    	cityMap.put(city.getId(), city.toString());
	    }
	    
	    return cityMap;
	}
}
