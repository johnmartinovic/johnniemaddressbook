package com.johnniem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.johnniem.dbdao.DBDAO;

/**
* <h1>Controller for other mappings</h1>
* 
* @author  JohnnieM
*/
@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
    private DBDAO dbDAO;
	
	/**
    * Show Homepage for empty queries
    */
	@GetMapping
	public String showHome() {
	    return "home";
	}
	
	/**
    * Create DB tables, functions and init table Sex (it is not modifiable)
    */
	@GetMapping("/createDB")
	public String createTables(
			Model model,
			final RedirectAttributes redirectAttributes) {
		dbDAO.createAllTables();
		
		dbDAO.createFunctions();
		
		dbDAO.initTableSex();
		
		boolean dbModified = true;
	    redirectAttributes.addFlashAttribute("dbModified", dbModified);
	    redirectAttributes.addFlashAttribute("dbModifiedMsg", "Tables created!");
	    
	    return "redirect:/";
	}
	
	/**
    * Fill tables with predefined data
    */
	@GetMapping("/addDBData")
	public String createRandomDBData(
			Model model,
			final RedirectAttributes redirectAttributes) {
		dbDAO.fillTablesWithValues();
		
		boolean dbModified = true;
	    redirectAttributes.addFlashAttribute("dbModified", dbModified);
	    redirectAttributes.addFlashAttribute("dbModifiedMsg", "Random data successfully created!");
	    
	    return "redirect:/";
	}
	
	/**
    * Drop all tables from DB
    */
	@GetMapping("/dropTables")
	public String dropTables(
			Model model,
			final RedirectAttributes redirectAttributes) {
	    dbDAO.deleteAllTables();
	    
	    boolean dbModified = true;
	    redirectAttributes.addFlashAttribute("dbModified", dbModified);
	    redirectAttributes.addFlashAttribute("dbModifiedMsg", "Tables deleted!");
	    
	    return "redirect:/";
	}
}