<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Contact - JohnnieMAddressBook</title>
	<link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' />
</head>
<body>
	<%@include file="header.html" %>
    <div class="form-style">
        <form:form action="${pageContext.request.contextPath}/contacts" method="post" modelAttribute="contactForm">
        	<fieldset>
				<legend>Contact</legend>
	        	<form:hidden path="id" />
            	<div class="bottomMargin">
            		<label for="firstNameId">First name:</label>
	                <form:input id="firstNameId" path="firstName" placeholder="Enter first name..."/>
	        		<form:errors path="firstName" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="lastNameId">Last name:</label>
	                <form:input id="lastNameId" path="lastName" placeholder="Enter last name..."/>
	        		<form:errors path="lastName" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="phoneId">Phone:</label>
	                <form:input id="phoneId" path="phone" placeholder="Enter phone number..."/>
	        		<form:errors path="phone" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="emailId">Email:</label>
	                <form:input id="emailId" path="email" placeholder="Enter email..."/>
	        		<form:errors path="email" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="sexId">Sex:</label>
            		<form:select id="sexId" path="sex">
   						<form:options items="${sexList}" />
					</form:select>
	        		<form:errors path="sex" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="streetId">Street name:</label>
	                <form:input id="streetId" path="street" placeholder="Enter street name..."/>
	        		<form:errors path="street" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="streetNoId">Street number:</label>
	                <form:input id="streetNoId" path="streetNo" placeholder="Enter street number..."/>
	        		<form:errors path="streetNo" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="cityIdId">City name:</label>
            		<form:select id="cityIdId" path="cityId">
   						<form:options items="${cityMap}" />
					</form:select>
	        		<form:errors path="cityId" cssClass="errorInput"/>
            	</div>
        		
                <input type="submit" value="Save"/>
            </fieldset>
        </form:form>
    </div>
</body>
</html>