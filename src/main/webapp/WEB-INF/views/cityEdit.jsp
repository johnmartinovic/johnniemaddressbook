<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>City - JohnnieMAddressBook</title>
	<link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' />
</head>
<body>
	<%@include file="header.html" %>
    <div class="form-style">
        <form:form action="${pageContext.request.contextPath}/cities" method="post" modelAttribute="cityForm">
        	<fieldset>
				<legend>City</legend>
	        	<form:hidden path="id" />
            	<div class="bottomMargin">
            		<label for="nameId">City name:</label>
	                <form:input id="nameId" path="name" placeholder="Enter name..."/>
	        		<form:errors path="name" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="zipCodeId">ZIP Code:</label>
	                <form:input id="zipCodeId" path="zipCode" placeholder="Enter ZIP Code..."/>
	        		<form:errors path="zipCode" cssClass="errorInput"/>
            	</div>
            	
            	<div class="bottomMargin">
            		<label for="countryId">Country name:</label>
            		<form:select id="countryId" path="countryId">
   						<form:options items="${countryMap}" />
					</form:select>
	        		<form:errors path="countryId" cssClass="errorInput"/>
            	</div>
        		
                <input type="submit" value="Save"/>
            </fieldset>
        </form:form>
    </div>
</body>
</html>