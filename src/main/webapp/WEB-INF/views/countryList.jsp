<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Countries - JohnnieMAddressBook</title>
        <link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' /> 
    </head>
    <body>
    	<%@include file="header.html" %>
	        <div class="list-style">
	        	<a class="button" href="countries/add">Create new country</a>
	            <h1 align="justify">
	            	List of countries
	            </h1>
	            <c:if test="${countryModified}">
	            	<h3 class="info-message">${countryModifiedMsg}</h3>
	            </c:if>
	            <table>
	                <th>No</th>
	                <th>Name</th>
	                <th>Alpha-2</th>
	                <th>Alpha-3</th>
	                <th colspan="2"></th>
	                 
	                <c:forEach var="country" items="${countryList}" varStatus="status">
	                <c:if test="${country.name == countryForm.name}"> 
  						<c:set value=" class=\"addedRow\"" var="cssClass"></c:set>
					</c:if> 
	                <tr${cssClass}>
	                    <td>${status.index + 1}</td>
	                    <td>${country.name}</td>
	                    <td>${country.alpha2}</td>
	                    <td>${country.alpha3}</td>
	                    <td><a class="button list-button" href="countries/${country.id}/update">Edit</a></td>
	                    <td><a class="button list-button" href="countries/${country.id}/delete">Delete</a></td>
	                </tr>
	                <c:set value="" var="cssClass"></c:set>
	                </c:forEach>             
	            </table>
	        </div>
    </body>
</html>