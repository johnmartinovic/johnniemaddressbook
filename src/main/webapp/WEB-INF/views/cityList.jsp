<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cities - JohnnieMAddressBook</title>
        <link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' /> 
    </head>
    <body>
    	<%@include file="header.html" %>
	        <div class="list-style">
	        	<a class="button" href="cities/add">Create new city</a>
	            <h1 align="justify">
	            	List of cities
	            </h1>
	            <c:if test="${cityModified}">
	            	<h3 class="info-message">${cityModifiedMsg}</h3>
	            </c:if>
	            <table>
	                <th>No</th>
	                <th>Name</th>
	                <th>ZIP Code</th>
	                <th>Country</th>
	                <th>Alpha-2</th>
	                <th>Alpha-3</th>
	                <th colspan="2"></th>
	                 
	                <c:forEach var="city" items="${cityList}" varStatus="status">
	                <c:choose>
	                	<c:when test="${city.name == cityForm.name}">
	                <tr class="addedRow">
  						</c:when>
  						<c:otherwise>
					<tr>
  						</c:otherwise>
	                </c:choose>
	                    <td>${status.index + 1}</td>
	                    <td>${city.name}</td>
	                    <td>${city.zipCode}</td>
	                    <td>${city.country.name}</td>
	                    <td>${city.country.alpha2}</td>
	                    <td>${city.country.alpha3}</td>
	                    <td><a class="button list-button" href="cities/${city.id}/update">Edit</a></td>
	                    <td><a class="button list-button" href="cities/${city.id}/delete">Delete</a></td>
	                </tr>
	                <c:set value="" var="cssClass"></c:set>
	                </c:forEach>             
	            </table>
	        </div>
    </body>
</html>