<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contacts - JohnnieMAddressBook</title>
        <link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' /> 
    </head>
    <body>
    	<%@include file="header.html" %>
	        <div class="list-style">
	        	<a class="button" href="contacts/add">Create new contact</a>
	            <h1 align="justify">
	            	List of contacts
	            </h1>
	            <c:if test="${contactModified}">
	            	<h3 class="info-message">${contactModifiedMsg}</h3>
	            </c:if>
	            <table>
	                <th>No</th>
	                <th>First name</th>
	                <th>Last name</th>
	                <th>Phone</th>
	                <th>Email</th>
	                <th>Sex</th>
	                <th>Street</th>
	                <th>Street No.</th>
	                <th>City</th>
	                <th>ZIP Code</th>
	                <th>Country</th>
	                <th>Alpha-2</th>
	                <th>Alpha-3</th>
	                <th colspan="2"></th>
	                 
	                <c:forEach var="contact" items="${contactList}" varStatus="status">
	                <c:if test="${contact.firstName == contactForm.firstName}"> 
  						<c:set value=" class=\"addedRow\"" var="cssClass"></c:set>
					</c:if> 
	                <tr${cssClass}>
	                    <td>${status.index + 1}</td>
	                    <td>${contact.firstName}</td>
	                    <td>${contact.lastName}</td>
	                    <td>${contact.phone}</td>
	                    <td>${contact.email}</td>
	                    <td>${contact.sex}</td>
	                    <td>${contact.street}</td>
	                    <td>${contact.streetNo}</td>
	                    <td>${contact.city.name}</td>
	                    <td>${contact.city.zipCode}</td>
	                    <td>${contact.city.country.name}</td>
	                    <td>${contact.city.country.alpha2}</td>
	                    <td>${contact.city.country.alpha3}</td>
	                    <td><a class="button list-button" href="contacts/${contact.id}/update">Edit</a></td>
	                    <td><a class="button list-button" href="contacts/${contact.id}/delete">Delete</a></td>
	                </tr>
	                <c:set value="" var="cssClass"></c:set>
	                </c:forEach>             
	            </table>
	        </div>
    </body>
</html>