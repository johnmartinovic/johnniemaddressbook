<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Country - JohnnieMAddressBook</title>
	<link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' />
</head>
<body>
	<%@include file="header.html" %>
    <div class="form-style">
        <form:form action="${pageContext.request.contextPath}/countries" method="post" modelAttribute="countryForm">
        	<fieldset>
				<legend>Country</legend>
	        	<form:hidden path="id" />
            	<div class="bottomMargin">
            		<label for="nameId">Country name:</label>
	                <form:input id="nameId" path="name" placeholder="Enter name..."/>
	        		<form:errors path="name" cssClass="errorInput"/>
            	</div>
        		
        		<div class="bottomMargin">
	                <label for="alpha2Id">Alpha-2 country code:</label>
	                <form:input id="alpha2Id" path="alpha2" placeholder="Enter alpha-2 code..."/>
	        		<form:errors path="alpha2" cssClass="errorInput"/>
            	</div>
        		
        		<div class="bottomMargin">
	        		<label for="alpha3Id">Alpha-3 country code:</label>
	                <form:input id="alpha3Id" path="alpha3" placeholder="Enter alpha-3 code..."/>
	        		<form:errors path="alpha3" cssClass="errorInput"/>
            	</div>
        		
                <input type="submit" value="Save"/>
            </fieldset>
        </form:form>
    </div>
</body>
</html>