<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>JohnnieMAddressBook</title>
	<link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' />
</head>
<body>
	<%@include file="header.html" %>
	<c:if test="${dbModified}">
		<h3 class="info-message">${dbModifiedMsg}</h3>
    </c:if>
    <div class="list-style">
    	<h1 align="center">
			Administrator options
		</h1>
		<h1 align="justify">
			In order to "Initialize DB", first we need to "Delete everything from the DB". Only after that, "Add random data" 
			can be executed.
		</h1>
		<a class="button" href="dropTables">Delete everything from the DB</a>
		<a class="button" href="createDB">Initialize DB</a>
		<a class="button" href="addDBData">Add random data</a>
	</div>
</body>
</html>