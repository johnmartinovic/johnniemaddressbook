# README #

JohnnieMAddressBook is a mini online address book. User can create and manipulate with countries, cities and contacts.

JohnnieMAddressBook can be tested [here](johnniem.com:8080/JohnnieMAddressBook).

## Further improvements ##
* 'delete' buttons should send POST requests instead of GET
* introduce some modern JavaScript libraries

## Keywords: ##
Java EE, Spring, PostgreSQL, SQL, HTML, CSS, JSP, Maven, Tomcat